behave features/scrum_project.feature -f json -o reports/report.json
behave features/scrum_project_team.feature -f json -o reports/report1.json
behave features/sprint.feature -f json -o reports/report2.json
behave features/sprint_backlog.feature -f json -o reports/report3.json
behave features/team_member.feature -f json -o reports/report4.json
behave features/user.feature -f json -o reports/report5.json
behave features/user_story.feature -f json -o reports/report6.json