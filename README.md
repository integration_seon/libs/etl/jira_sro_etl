# Jira_sro_etl

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=integration_seon_jira_sro_etl&metric=alert_status)](https://sonarcloud.io/dashboard?id=integration_seon_jira_sro_etl) [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=integration_seon_jira_sro_etl&metric=coverage)](https://sonarcloud.io/dashboard?id=integration_seon_jira_sro_etl) [![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=integration_seon_jira_sro_etl&metric=code_smells)](https://sonarcloud.io/dashboard?id=integration_seon_jira_sro_etl)

## General Information
* **Software**:jira_sro_etl
* **Author**:Leonardo Khoury Picoli, Lucas Moraes Soares de Souza, Paulo Sérgio dos Santos Júnior
* **Author's e-mail**:leonardokhoury@hotmail.com, lucasmoraesplay@gmail.com, paulossjunior@gmail.com
* **Source Repository**: [https://gitlab.com/integration_seon/libs/etl/jira_sro_etl](https://gitlab.com/integration_seon/libs/etl/jira_sro_etl)  

## Goal
Library responsible for extracting data from Jira to SRO database

## Documentation

The Documentation can be found in this [link](./docs/README.md)

## Requirements

The code need the following programs to run, versions were listed just for guidance, you can try with different ones by your own risk, we tested with these.

| Program               | Version |
| --------------------- | ------: |
| Postgres              | 13.3    |
| MongoDB Community     | 4.4.5   |
| Python                | 3.8.5   |
| Pip                   | 21.1.1  |


## Installation

If you have already made the initial fill, just install jira_sro_etl running:
```bash
pip install jira_sro_etl
```

If you wanna do the initial fill, clone this repo and follow the steps listed on USAGE section

## Usage

Since it uses SRO_DB lib you must export the following environment variables:

* USERDB = Postgres's username
* PASSWORDDB = Postgres's password
* HOST = Postgres's host (usually localhost)
* DBNAME = Your database's name (must be already created) on Postgres

And if your mongo db is not on your localhost you can export:

* MongoDB = Mongo host address

Create a virtual environment and activate it
```shell
python3 -m venv env && source env/bin/activate
```

Install the requirements
```shell
pip install -r requirements.txt
```

Fill the fields on dump_db/create_org.py and run
```shell
python dump_db/create_db.py && python dump_db/create_org.py
```

Grab the data that was printed on terminal after last command and paste it on initial_fill.py (before the `manager = Manager(data)`), then run
```shell
python initial_fill.py
```

---

After the initial fill you can run new creations, updates or deletes on any classes. If you've installed jira_sro_etl with pip you can run the following code for example:
```python
from jira_sro_etl import factories
u = factories.userFactory()
u.create({'content': {'all': {'user': {'accountId': 'ACCOUNT_ID_FROM_JIRA'}}}})

```

## CI/CD configuration

Configure the following variables in CI/CD:

* VAR_SONAR_CLOUD_TOKEN = Token from SonarCloud
* VAR_GITLAB_USER_NAME = username of Gitlab
* VAR_GITLAB_USER_EMAIL = user's email of Gitlab
* VAR_PYPI_USER_NAME = pypi's username
* VAR_PYPI_PASSWORD = pypi's password 

## Copyright
This lib was PowerRight by [SEON Application Lib Generator](https://gitlab.com/mdd_seon/from_application_conceptual_data_model_2_lib_application)

	
