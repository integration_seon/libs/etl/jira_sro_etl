from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='jira_sro_etl',  # Required
    version='26.0.0',  # Required
    author="Leonardo Khoury Picoli, Lucas Moraes Soares de Souza, Paulo Sérgio dos Santos Júnior",
    author_email="leonardokhoury@hotmail.com, lucasmoraesplay@gmail.com, paulossjunior@gmail.com",
    description="Library responsible for extracting data from Jira to SRO database",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/integration_seon/libs/etl/jira_sro_etl",
    packages=find_packages(),
    
    install_requires=[
        'behave', 'commitizen','factory-boy'
    ],

    classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
    setup_requires=['wheel'],
    
)


