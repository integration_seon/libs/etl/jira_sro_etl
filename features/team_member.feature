Feature: Retrive all team_member of a organization

    Scenario: get all team_member
    Given a organization with team_member
    When i request a team_member
    Then i save a list of team_members on data base

    #Fail test - just testing if the organization is wrong
    Scenario: fail get all team_member
    Given a false organization_team_member
    When i request a false team_member
    Then i fail in save a list of team_members on data base
