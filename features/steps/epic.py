from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global epic

@given(u'a organization with epic')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a epic')
def step_impl(context):
    global epic
    epic = factories.epicFactory()


@then(u'i save a list of epics  on data base')
def step_impl(context):
    epic.do(data)

#------Fail-----
@given(u'a false organization_epic')
def step_impl(context):
    global data
    epic = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'epic': epic,
        'key': apikey,
        'url': server
    }

@when(u'i request a false epic')
def step_impl(context):
    global epic
    epic = factories.epicFactory()


@then(u'i fail in save a list of epics on data base')
def step_impl(context):
   try:       epic.do(data)
   except Exception as e:
       pass
