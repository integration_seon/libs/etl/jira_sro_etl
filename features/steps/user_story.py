from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global user_story

@given(u'a organization with user_story')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a user_story')
def step_impl(context):
    global user_story
    user_story = factories.user_storyFactory()


@then(u'i save a list of user_storys  on data base')
def step_impl(context):
    user_story.do(data)

#------Fail-----
@given(u'a false organization_user_story')
def step_impl(context):
    global data
    user_story = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'user_story': user_story,
        'key': apikey,
        'url': server
    }

@when(u'i request a false user_story')
def step_impl(context):
    global user_story
    user_story = factories.user_storyFactory()


@then(u'i fail in save a list of user_storys on data base')
def step_impl(context):
   try:       user_story.do(data)
   except Exception as e:
       pass
