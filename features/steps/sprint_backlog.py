from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global sprint_backlog

@given(u'a organization with sprint_backlog')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a sprint_backlog')
def step_impl(context):
    global sprint_backlog
    sprint_backlog = factories.sprint_backlogFactory()


@then(u'i save a list of sprint_backlogs  on data base')
def step_impl(context):
    try: #Temporary
        sprint_backlog.do(data)
    except Exception as e:
        pass

#------Fail-----
@given(u'a false organization_sprint_backlog')
def step_impl(context):
    global data
    sprint_backlog = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'sprint_backlog': sprint_backlog,
        'key': apikey,
        'url': server
    }

@when(u'i request a false sprint_backlog')
def step_impl(context):
    global sprint_backlog
    sprint_backlog = factories.sprint_backlogFactory()


@then(u'i fail in save a list of sprint_backlogs on data base')
def step_impl(context):
   try:       
       sprint_backlog.do(data)
   except Exception as e:
       pass
