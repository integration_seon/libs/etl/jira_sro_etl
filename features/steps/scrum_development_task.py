from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global scrum_development_task

@given(u'a organization with scrum_development_task')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a scrum_development_task')
def step_impl(context):
    global scrum_development_task
    scrum_development_task = factories.scrum_development_taskFactory()


@then(u'i save a list of scrum_development_tasks  on data base')
def step_impl(context):
    scrum_development_task.do(data)

#------Fail-----
@given(u'a false organization_scrum_development_task')
def step_impl(context):
    global data
    scrum_development_task = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'scrum_development_task': scrum_development_task,
        'key': apikey,
        'url': server
    }

@when(u'i request a false scrum_development_task')
def step_impl(context):
    global scrum_development_task
    scrum_development_task = factories.scrum_development_taskFactory()


@then(u'i fail in save a list of scrum_development_tasks on data base')
def step_impl(context):
   try:       scrum_development_task.do(data)
   except Exception as e:
       pass
