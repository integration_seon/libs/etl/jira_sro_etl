import logging
from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global scrum_project

@given(u'a organization with scrum_project')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a scrum_project')
def step_impl(context):
    global scrum_project
    try:
        scrum_project = factories.scrum_projectFactory()
    except Exception as e:
        logging.info(e)
    


@then(u'i save a list of scrum_projects  on data base')
def step_impl(context):
    try:
        assert scrum_project.do(data)
    except Exception as e:
        logging.info(e)

#------Fail-----
@given(u'a false organization_scrum_project')
def step_impl(context):
    global data
    scrum_project = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'scrum_project': scrum_project,
        'key': apikey,
        'url': server
    }

@when(u'i request a false scrum_project')
def step_impl(context):
    global scrum_project
    scrum_project = factories.scrum_projectFactory()


@then(u'i fail in save a list of scrum_projects on data base')
def step_impl(context):
   try:       scrum_project.do(data)
   except Exception as e:
       pass
