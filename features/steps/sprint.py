from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global sprint

@given(u'a organization with sprint')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a sprint')
def step_impl(context):
    global sprint
    sprint = factories.sprintFactory()


@then(u'i save a list of sprints  on data base')
def step_impl(context):
    sprint.do(data)

#------Fail-----
@given(u'a false organization_sprint')
def step_impl(context):
    global data
    sprint = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'sprint': sprint,
        'key': apikey,
        'url': server
    }

@when(u'i request a false sprint')
def step_impl(context):
    global sprint
    sprint = factories.sprintFactory()


@then(u'i fail in save a list of sprints on data base')
def step_impl(context):
   try:       sprint.do(data)
   except Exception as e:
       pass
