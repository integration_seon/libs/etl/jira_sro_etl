from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global user

@given(u'a organization with user')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a user')
def step_impl(context):
    global user
    user = factories.userFactory()


@then(u'i save a list of users  on data base')
def step_impl(context):
    user.do(data)

#------Fail-----
@given(u'a false organization_user')
def step_impl(context):
    global data
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'user': user,
        'key': apikey,
        'url': server
    }

@when(u'i request a false user')
def step_impl(context):
    global user
    user = factories.userFactory()


@then(u'i fail in save a list of users on data base')
def step_impl(context):
   try:       user.do(data)
   except Exception as e:
       pass
