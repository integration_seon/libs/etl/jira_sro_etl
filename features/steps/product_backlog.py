from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global product_backlog

@given(u'a organization with product_backlog')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a product_backlog')
def step_impl(context):
    global product_backlog
    product_backlog = factories.product_backlogFactory()


@then(u'i save a list of product_backlogs  on data base')
def step_impl(context):
    product_backlog.do(data)

#------Fail-----
@given(u'a false organization_product_backlog')
def step_impl(context):
    global data
    product_backlog = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'product_backlog': product_backlog,
        'key': apikey,
        'url': server
    }

@when(u'i request a false product_backlog')
def step_impl(context):
    global product_backlog
    product_backlog = factories.product_backlogFactory()


@then(u'i fail in save a list of product_backlogs on data base')
def step_impl(context):
   try:       
       product_backlog.do(data)
   except Exception as e:
       pass
