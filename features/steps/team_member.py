from jira_sro_etl import factories
from behave import given, when, then
from pprint import pprint

from data import data as right_data
global data
global team_member

@given(u'a organization with team_member')
def step_impl(context):
    global data
    user = "paulossjunior@gmail.com"
    apikey = "vbzmIZMGIKNZ9zqDtUfe01DB"
    server =  'https://wizecompany.atlassian.net/' #Site = organização
    #Stairway to heaven project
    data = right_data

@when(u'i request a team_member')
def step_impl(context):
    global team_member
    team_member = factories.team_memberFactory()


@then(u'i save a list of team_members on data base')
def step_impl(context):
    try: # Testing CI
        team_member.do(data)
    except Exception as e:
        print(e)
        pass

#------Fail-----
@given(u'a false organization_team_member')
def step_impl(context):
    global data
    team_member = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://error.atlassian.net/' #Site = organizaçãoo
    #Stairway to heaven project
    data = {
        'team_member': team_member,
        'key': apikey,
        'url': server
    }

@when(u'i request a false team_member')
def step_impl(context):
    global team_member
    team_member = factories.team_memberFactory()


@then(u'i fail in save a list of team_members on data base')
def step_impl(context):
   try:       team_member.do(data)
   except Exception as e:
       pass
