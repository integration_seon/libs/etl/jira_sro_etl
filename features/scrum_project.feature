Feature: Retrive all scrum_project of a organization

    Scenario: get all scrum_project
    Given a organization with scrum_project
    When i request a scrum_project
    Then i save a list of scrum_projects  on data base

    #Fail test - just testing if the organization is wrong
    Scenario: fail get all scrum_project
    Given a false organization_scrum_project
    When i request a false scrum_project
    Then i fail in save a list of scrum_projects on data base
