Feature: Retrive all scrum_development_task of a organization

    Scenario: get all scrum_development_task
    Given a organization with scrum_development_task
    When i request a scrum_development_task
    Then i save a list of scrum_development_tasks  on data base

    #Fail test - just testing if the organization is wrong
    Scenario: fail get all scrum_development_task
    Given a false organization_scrum_development_task
    When i request a false scrum_development_task
    Then i fail in save a list of scrum_development_tasks on data base
