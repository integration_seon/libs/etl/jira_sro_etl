Feature: Retrive all scrum_project_team of a organization

    Scenario: get all scrum_project_team
    Given a organization with scrum_project_team
    When i request a scrum_project_team
    Then i save a list of scrum_project_teams  on data base

    #Fail test - just testing if the organization is wrong
    Scenario: fail get all scrum_project_team
    Given a false organization_scrum_project_team
    When i request a false scrum_project_team
    Then i fail in save a list of scrum_project_teams on data base
