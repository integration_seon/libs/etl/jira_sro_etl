# Documentation

## Application Conceptual Data Model
![Domain Diagram](classdiagram.png)	
	
## Entities
	
* **product_backlog** : Product backlog
* **scrum_development_task** : Scrum performed development task
* **scrum_project** : Scrum project
* **scrum_project_team** : Scrum project team
* **scrum_development_team** : Scrum development team
* **sprint** : Sprint (Time box)
* **team_member** : Team member of a project
* **user_story** : User story
* **epic** : An epic
* **User** : User
* **sprint_backlog** : Sprint backlog

		
## Copyright
This lib was PowerRight by [SEON Application Lib Generator](https://gitlab.com/mdd_seon/from_application_conceptual_data_model_2_lib_application) 
