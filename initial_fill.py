from jira_sro_etl.manager import Manager

import logging
# logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger()
logger.setLevel(level=logging.ERROR)

from datetime import datetime
start_time = datetime.now()


user = ''   # Jira e-mail
apikey = '' # Jira ApiKey
server = 'https://YOUR_DOMAIN.atlassian.net/' # Jira Server

#data = {
#    'user': 'paulossjunior@gmail.com',
#    'key': 'vbzmIZMGIKNZ9zqDtUfe01DB',
#    'url': 'https://wizecompany.atlassian.net/',
#    'organization_id': '8bbaf701-9980-415e-8a01-891f276b5da8',
#    'configuration_id': 'c081f55c-cfc8-456d-96b3-b53b1b591c21',
#}


data = {
    'user': 'paulossjunior@gmail.com',
    'key': 'vbzmIZMGIKNZ9zqDtUfe01DB',
    'url': 'https://wizecompany.atlassian.net/',
    'organization_id': '7b8d129a-afdf-4fea-8612-98efa2199069',
    'configuration_id': '6ddb396a-5ea9-44a1-aa54-839adbbf5217',
}

manager = Manager(data)
manager.extract_all()
manager.do_all()

end_time = datetime.now()
print('Elapsed Extract time: {}'.format(end_time - start_time))
