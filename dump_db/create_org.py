from sro_db.application import factories as application_factory
from sro_db.model import factories as model_factory


#Criando os dados do prodest
organization = model_factory.OrganizationFactory()
organization.name = "ORGANIZATION NAME"
organization.description = "DESCRIPTION"

organization_application = application_factory.OrganizationFactory()
organization = organization_application.create(organization)

applicationService = application_factory.ApplicationFactory()
jira = applicationService.retrive_by_name("jira")

#criando a applicação
configuration = model_factory.ConfigurationFactory()
configuration.name = f"{organization.name}'s configuration for jira Jira"
configuration.description = f"{organization.name}'s configuration for jira Jira"
configuration.user = "YOUR JIRA USERNAME"
configuration.secret = "YOUR JIRA API KEY"
# If you don't know how to get the api key, follow the steps on the link bellow:
# https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/
configuration.url = "YOUR ORGANIZATION URL" # usually: https://YOUR_COMPANY.atlassian.net/

configuration.application = jira
configuration.organization = organization

configuration_application = application_factory.ConfigurationFactory()
configuration = configuration_application.create(configuration)

print('data = {')
print(f"    'user': '{configuration.user}',")
print(f"    'key': '{configuration.secret}',")
print(f"    'url': '{configuration.url}',")
print(f"    'organization_id': '{str(organization.uuid)}',")
print(f"    'configuration_id': '{str(configuration.uuid)}',")
print("}")
